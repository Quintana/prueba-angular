import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ColumnMode } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  preserveWhitespaces: true
})
export class DashboardComponent implements OnInit {

  page: any = {
    size:  0,
    totalElements:  0,
    totalPages:  0,
    pageNumber:  0
  };

  rows = [];
  ColumnMode = ColumnMode;

  posts: any;

  url: string = 'https://gorest.co.in/public/v1/users';

  constructor(private http: HttpClient) {
    this.page.pageNumber = 0;
    this.page.size = 10;
  }

  ngOnInit(): void {
    this.setPage({ offset: 0 });
  }

  // @ts-ignore
  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;

    this.http.get(this.page.pageNumber < 1 ? this.url : this.url + '?page=' + (this.page.pageNumber+1)).subscribe(posts => {
      this.posts = posts;
      // @ts-ignore
      this.page.size = posts.meta.pagination.limit;
      // @ts-ignore
      this.page.totalElements = posts.meta.pagination.total;
      // @ts-ignore
      this.page.totalPages = posts.meta.pagination.pages;
      // @ts-ignore
      this.rows = posts.data;
    }, error => console.error(error));
  }
}

